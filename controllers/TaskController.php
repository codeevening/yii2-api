<?php
namespace api\controllers;

use api\components\rest\Controller;
use api\models\db\Task;
use api\models\searches\SearchTask;
use yii\rest\Serializer;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use Yii;

class TaskController extends Controller
{
    public function actionIndex(){
        $model = new SearchTask();
        $dataProvider = $model->search(Yii::$app->request->getQueryParams());
        $serializer = new Serializer();
        return $serializer->serialize($dataProvider);
    }

    public function actionView($id){
        $task = $this->findModel($id);
        $model = new SearchTask();
        $dataProvider = $model->search(["id" => $task->id]);
        $serializer = (new Serializer())->serialize($dataProvider);
        return current($serializer);
    }

    public function actionCreate(){
        $task = new Task();
        if ($task->load(Yii::$app->request->getBodyParams(), "")){
            if ($task->save()) {
                Yii::$app->response->statusCode = 201;
                return $this->actionView($task->id);
            }else{
                throw new BadRequestHttpException(current($task->getFirstErrors()));
            }
        }
        throw new BadRequestHttpException("Не заполнены данные");
    }

    public function actionUpdate($id){
        $task = $this->findModel($id);
        if ($task->load(Yii::$app->request->getBodyParams(), "")){
            if ($task->save()) {
                return $this->actionView($task->id);
            }else{
                throw new BadRequestHttpException(current($task->getFirstErrors()));
            }
        }
        throw new BadRequestHttpException("Не заполнены данные");
    }

    public function actionDelete($id){
        $task = $this->findModel($id);
        $task->delete();
        Yii::$app->response->statusCode = 204;
        $model = new SearchTask();
        $dataProvider = $model->search(["id" => $task->id]);
        $serializer = new Serializer();
        return $serializer->serialize($dataProvider);
    }

    private function findModel($id){
        if (($model = Task::findOne($id)) != null){
            return $model;
        }
        throw new NotFoundHttpException("Таск не найден");
    }
}