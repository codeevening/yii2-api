<?php
/**
 * Created by PhpStorm.
 * User: TimJ
 * Date: 31.08.2018
 * Time: 9:46
 */

namespace api\models\db;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Task extends ActiveRecord
{
    /**
     * This is the model class for table "{{%task}}".
     *
     * @property int $id ID
     * @property string $name Название
     * @property string $state Состояние
     * @property int $created_at Добавлено
     * @property int $updated_at Добавлено
     */

    public function beforeValidate()
    {
        if (!is_null($this->state)) $this->state = intval($this->state);
        return parent::beforeValidate();
    }

    public function beforeSave($insert)
    {
        if (empty($this->state)) $this->state = 0;
        if (empty($this->created_at)) $this->created_at = time();
        if (empty($this->updated_at)) $this->updated_at = time();
        return parent::beforeSave($insert);
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['created_at', 'updated_at'],
                ],
            ],
        ];
    }

    public static function tableName()
    {
        return '{{task}}';
    }

    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['state'], 'integer'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'state' => 'Состояние',
            'created_at' => 'Добавлено',
            'updated_at' => 'Изменено'
        ];
    }
}