<?php
namespace api\models\searches;

use api\models\db\Task;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use Yii;

class SearchTask extends Task
{

    public function rules()
    {
        return [
            [["id", "state"], "integer"],
            [["name"], "string"]
        ];
    }

    public function scenarios()
    {
        return Model::scenarios();
    }

    public function fields()
    {
        return [
            "id",
            "name",

        ];
    }

    public function extraFields()
    {
        return [
            "state",
            "created_at" => function($model){
                return Yii::$app->formatter->asDatetime($model->created_at, "php:d M Y H:i");
            },
            "updated_at" => function($model){
                return Yii::$app->formatter->asDatetime($model->updated_at, "php:d M Y H:i");
            }
        ];
    }

    public function search($params)
    {
        $query = self::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            "pagination" => [
                "pageSize" => 5
            ],
            'sort' => [
                'defaultOrder' => [
                    'updated_at' => SORT_DESC
                ]
            ]
        ]);

        $this->load($params, "");

        if (!$this->validate()) {
            return $dataProvider;
        }


        $query
            ->orFilterWhere([
                "id" => $this->id,
                "state" => $this->state
            ])
            ->orFilterWhere(['like', 'name', $this->name]);
        return $dataProvider;
    }
}