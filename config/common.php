<?php
    return [
        'controllerNamespace' => 'api\controllers',
        'sourceLanguage' => 'ru',
        'language' => 'ru',
        'id' => 'yii2-api-console',
        'basePath' => dirname(__DIR__),
        'aliases' => [
            '@api' => __DIR__ . "/../",
            '@runtime' => __DIR__ . "/runtime/"
        ],
        'components' => [
            'db' => [
                'class' => 'yii\db\Connection',
                'dsn' => 'sqlite:@api/db/database.sqlite',
            ],
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['info'],
                    ],
                ],
            ],
        ],

    ];