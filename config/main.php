<?php
use yii\web\Response;
return [
        'components' => [
            'response' => [
                'class' => 'yii\web\Response',
                'on beforeSend' => function ($event) {
                    $response = $event->sender;
                    if ($response->format != "html") {
                        $body = $response->data;

                        $data = [
                            "data" => $response->data,
                            "code" => $response->statusCode,
                        ];
                        if ($response->statusCode >= 300) {
                            $data["message"] = $response->data["message"];
                        }
                        $data["data"] = $body;
                        if ($response->data !== null) {
                            $data["code"] = $response->statusCode;
                            if (YII_DEBUG) {
                                $data['elapsed_time'] = Yii::getLogger()->getElapsedTime();
                                $sqls = Yii::getLogger()->getProfiling([
                                    'yii\db\Command::query',
                                    'yii\db\Command::execute'
                                ]);

                                $data["sql"] = array_map(function ($sql) {
                                    return [
                                        $sql['info'],
                                        $sql['duration'],
                                    ];
                                }, $sqls);
                            }
                        }
                        $response->data = $data;
                    }
                }
            ],
            'request' => [
                'enableCookieValidation' => false,
                'enableCsrfValidation' => false,
                'parsers' => [
                    '' => 'yii\web\JsonParser',
                    'application/json' => 'yii\web\JsonParser',
                    'application/xml' => 'light\yii2\XmlParser',
                ]
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
            ],
            'urlManager' => [
                'class' => 'yii\web\UrlManager',
                'enablePrettyUrl' => true,
                'showScriptName' => false,
                'enableStrictParsing' => true,
                'rules' => [
                    "OPTIONS <path:.*>" => "task/options",
                    "GET task/<id:\d+>" => "task/view",
                    "GET task" => "task/index",
                    "POST task" => "task/create",
                    "PUT task/<id:\d+>" => "task/update",
                    "DELETE task/<id:\d+>" => "task/delete"
                ]
            ]
        ],
        'bootstrap' => [
            [
                'class' => 'yii\filters\ContentNegotiator',
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                    'application/xml' => Response::FORMAT_XML,
                ]
            ],
        ],
        "params" => [

        ]
    ];
