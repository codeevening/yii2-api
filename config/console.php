<?php
return [
    'controllerMap' => [
        'migrate' => [
            'migrationPath' => '@api/migrations',
            'class' => '\yii\console\controllers\MigrateController'
        ],
    ],
    'params' => [

    ]
];