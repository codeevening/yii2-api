var app = new Vue({
	el: '#app',
	data: {
		endpoint: 'http://api.ce.local',
		tasks: [],
		pages: [],
		current: 1
	},
	mounted: function(){
		this.getPages();
	},

	methods: {
		__renderPagination: function(response){
			var count = response.headers.get("X-Pagination-Page-Count"),
				current = response.headers.get("X-Pagination-Current-Page");
			if (count){
				this.pages = [];
				for (var i=1; i<=count; i++){
					this.pages.push(i);
				}
			}
			if (current) {
				this.current = current;
			}
		},
		getPages: function(page){
			if (!page) page = 1;
			var self = this;
			fetch(this.$root.endpoint + "/task?expand=state&page="+page, {
				method: "GET",
				headers: {
					'Content-Type': 'application/json'
				}
			}).then(function(response) {
				self.__renderPagination(response);
				return response.json();
			})
				.then(function(data) {
					if (data.data){
						self.tasks = data.data;
					}
				})
				.catch( alert );
		},
		create: function(event){
			var value = event.target.value;
			if (value.trim().length){
				var self = this;
				fetch(this.$root.endpoint + "/task?expand=state,updated_at", {
					method: "POST",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						name: value,
						state: 1
					})
				}).then(function(response) {
					if (response.status == 201) {
						return response.json();
					}
				})
				.then(function(data) {
					self.tasks.unshift(data.data);
					event.target.value = "";
				})
				.catch( alert );
			}
		},
		remove: function(id){
			var index = null;
			for (var i=0; i<this.tasks.length; i++){
				if (this.tasks[i].id == id){
					index = i;
					break;
				}
			}
			if (index !== null){
				self = this;
				fetch(this.$root.endpoint + "/task/" + id, {
					method: "DELETE",
					headers: {
						'Content-Type': 'application/json'
					}
				}).then(function(response) {
					if (response.status == 204) {
						self.tasks.splice(index, 1);
					}
				})
				.catch( alert );
			}
		},
		changeState: function (id) {
			var index = null;
			for (var i=0; i<this.tasks.length; i++){
				if (this.tasks[i].id == id){
					index = i;
					break;
				}
			}
			if (index !== null){
				var state = this.tasks[index].state,
					self = this;
				fetch(this.$root.endpoint + "/task/" + id + "?expand=state,updated_at", {
					method: "PUT",
					headers: {
						'Content-Type': 'application/json'
					},
					body: JSON.stringify({
						state: !parseInt(state)
					})
				}).then(function(response) {
					return response.json();
				})
				.then(function(data) {
					if (data.data){
						self.tasks[index].state = data.data.state;
					}
				})
				.catch( alert );
			}
		}
	}
});